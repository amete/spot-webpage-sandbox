<?php

// Get common funtions
require_once('dependencies/base.php');

// Find the subfolders for the menu
$menu=Array();
$cfg_tests=Array();
$test2info=Array();
$sortedresults=glob('pages/*/*/html.snippet');
usort($sortedresults,"comparePages");
foreach($sortedresults as $d) {
  $vals=split('/',$d);
  $metric=$vals[1];   // e.g. cpu, malloc etc.
  $relplat=$vals[2];  // e.g. master__x86_64-centos7-gcc8-opt etc.

  $testkey="${metric}/${relplat}"; // e.g cpu/master__x86_64-centos7-gcc8-opt
  $pretty="${metric} (".str_replace('__','/',$relplat).')';
  $test2info[$testkey]=Array('snippet'=>$d,'pretty'=>$pretty);
  array_push($cfg_tests,$testkey);

  if (!array_key_exists($metric,$menu)) {
    $menu[$metric]=Array();
  }
  if (!array_key_exists($release,$menu[$metric])) {
    $menu[$metric][$relplat]=Array();
  }
  $menu[$metric][$relplat] = $testkey;

}

// Build the page here...

// First comes the header
spotheader('ATLAS SPOT Monitoring Pages');

// Then comes the top navigation bar
navbar($menu);
print '<br><br><br>'."\n"; // ad-hoc space for now

// Print the html snippet
$cfg_test=getVar('test','summary',$cfg_tests);
$htmlsnippet=$test2info[$cfg_test]['snippet'];
printTable($htmlsnippet,true,dirname($htmlsnippet));

// Lastly we have the footer
print '<br>'."\n"; // ad-hoc space for now
spotfooter();

?>
