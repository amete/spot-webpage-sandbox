<?php

// Define the page header...
function spotheader($title) {
  print '<html>'."\n";
  print '<head>'."\n";
  print '  <title>'."$title".'</title>'."\n";
  print '  <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,300" rel="stylesheet" type="text/css">'."\n";
  print '  <link rel="icon" href="../../atlas-logo-new.png" type="image/x-icon" />'."\n";
  print '  <link rel="stylesheet" type="text/css" href="dependencies/font-awesome/5.15.3/css/all.css">'."\n";
  print '  <link rel="stylesheet" type="text/css" href="dependencies/styles.css" />'."\n";
  print '  <script src="dependencies/scripts.js"></script>'."\n";
  print '</head>'."\n";
  print '<body>'."\n";
}

// Define the page footer...
function spotfooter() {
  print '<div class="footer">'."\n";
  print '  Copyright <i class="fa fa-copyright"></i> 2022 CERN for the benefit of the ATLAS collaboration. Designed by Alaettin Serhan Mete.'."\n";
  print '</div>'."\n";
  print '</body>'."\n";
  print '</html>'."\n";
}

// Print the navigation bar
function navbar($menu) {
  print '<ul class="navbar">'."\n";
  foreach($menu as $metric=>$release) {
    print '  <div class="dropdown">'."\n";
    $thetext=getNiceMetricName($metric);
    print '    <button class="dropbtn">'.getMenuIcon($metric).' '.$thetext.'</button>'."\n";
    print '      <div class="dropdown-content">'."\n";
    foreach($release as $key=>$value) { // value is release/platform
      $thelink='?test='.$value;
      $thesubtext=str_replace('__','/',split('/',$value)[1]);
      print '        <a href="'.$thelink.'">'.$thesubtext.'</a>'."\n";
    }
    print '      </div>'."\n";
    print '  </div>'."\n";
  }
  // Contact
  print '  <div class="dropdown">'."\n";
  print '    <button class="dropbtn">'.getMenuIcon('email').' Contact</button>'."\n";
  print '      <div class="dropdown-content">'."\n";
  print '        <a href="mailto:atlas-computing-spot@cern.ch">ATLAS SPOT Mailing List</a>'."\n";
  print '      </div>'."\n";
  print '  </div>'."\n";
  print '</ul>'."\n";
}

// Print table
function printTable($tablefile,$correctlastupdate=true,$translatelinks='') {
  $tl=($translatelinks!='');
  $hreftarget='href="'.$translatelinks.'/';
  $srctarget='src="'.$translatelinks.'/';
  if($fh = fopen($tablefile,"r")){
    while(!feof($fh)) {
      $line=fgets($fh);
      if ($tl) {
        if (strstr($line,'href=')) {
          $line=str_replace('href="http','HREF="http',$line);
          $line=str_replace('href="!','HREF="',$line);
          $line=str_replace('href="',$hreftarget,$line);
          $line=str_replace('HREF="','href="',$line);
        }
        if (strstr($line,'src=')) {
          $line=str_replace('src="http','SRC="http',$line);
          $line=str_replace('src="!','SRC="',$line);
          $line=str_replace('src="',$srctarget,$line);
          $line=str_replace('SRC="','src="',$line);
        }
      }
      $i=($correctlastupdate&&strpos($line,'LASTUPDATE['));
      if ($i===false) {
        print $line;
            } else {
        $parts=explode('LASTUPDATE[',$line,2);
        $parts2=explode(']',$parts[1],2);
        print $parts[0].nicetime($parts2[0]).$parts2[1];
      }
    }
    fclose($fh);
  }
}

function getVar($key,$default,$allowedvalues) {
  if (!array_key_exists($key,$_GET)) {
    if (in_array($default,$allowedvalues)) {
      return $default;
    }
    return $allowedvalues[0];
  }
  $val=$_GET[$key];
  if (!in_array($val,$allowedvalues)) {
    print "ERROR: $key=$val not recognised. Problem might be temporary so try to reload in a few seconds.\n";
    exit(1);
  }
  return $val;
}

// Helper function to sort the menu
function getReleaseOrder($input) {
  switch($input) {
    case '23.0':
        return 0;
    case 'main':
        return 1;
    case 'master':
        return 2;
    case 'master--archflagtest';
        return 3;
    case 'master--ltoflagtest';
        return 4;
    case 'master--HepMC2';
        return 5;
  }
  return 99;
}

function getPlatformOrder($input) {
  switch($input) {
    case 'x86_64-centos7-gcc11-opt':
        return 0;
    case 'x86_64-centos7-clang16-opt':
        return 1;
  }
  return 99;
}

function getMetricOrder($input) {
  switch($input) {
    case 'summary':
        return 0;
    case 'cpu';
        return 1;
    case 'malloc';
        return 2;
    case 'vmem';
        return 3;
    case 'sizeperevt';
        return 4;
  }
  return 99;
}

// Sort pages for the menu
function comparePages($val1, $val2) {
    # Parse the first argument
    $tokens1 = split('/', $val1);
    $metric1 = getMetricOrder($tokens1[1]);
    $release1 = getReleaseOrder(split('__',$tokens1[2])[0]);
    $platform1 = getPlatformOrder(split('__',$tokens1[2])[1]);

    # Parse the second argument
    $tokens2 = split('/', $val2);
    $metric2 = getMetricOrder($tokens2[1]);
    $release2 = getReleaseOrder(split('__',$tokens2[2])[0]);
    $platform2 = getPlatformOrder(split('__',$tokens2[2])[1]);

    # First by metric, then by release, then by platform
    # For now there is no custom platform order...
    if ($metric1 == $metric2) {
        if ($release1 == $release2) {
            return ($platform1 < $platform2) ? -1 : 1;
        }
        return ($release1 < $release2) ? -1 : 1;
    }
    return ($metric1 < $metric2) ? -1 : 1;
}

// Convert folder names to nicer ones
function getNiceMetricName($input) {
  switch($input) {
    case 'summary':
        return 'Overview';
    case 'cpu';
        return 'CPU Utilization';
    case 'malloc';
        return 'Memory Allocations (malloc)';
    case 'vmem';
        return 'Memory Allocations (vmem)';
    case 'sizeperevt';
        return 'Container Sizes';
  }
  return $input;
}

// Get icons for the menu
function getMenuIcon($input) {
  switch($input) {
    case 'summary':
        return '';
    case 'cpu';
        return '<i class="fa fa-microchip"></i>';
    case 'malloc';
        return '<i class="fa fa-memory"></i>';
    case 'vmem';
        return '<i class="fa fa-memory"></i>';
    case 'sizeperevt';
        return '<i class="fa fa-database"></i>';
    case 'email';
        return '<i class="fa fa-envelope"></i>';
  }
  return '';
}

?>
